<?php
 /**
 * @package jlvklike
 * @author Vadim Kunicin (vadim@joomline.ru), Artem Jukov (artem@joomline.ru)  Anton Voynov (anton@joomline.net)
 * @version 2.1
 * @copyright (C) 2012 by Joomline(http://www.joomline.ru)
 * @license JoomLine: http://joomline.net/licenzija-joomline.html
 *
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );
jimport ('joomla.html.parameter');


class plgContentJlvklike extends JPlugin
{

	function plgContentJlvklike( &$subject, $params )
	{
		parent::__construct( $subject, $params );
		$plugin			=& JPluginHelper::getPlugin('content', 'jlvklike');
		$this->params	= new JRegistry( $plugin->params );

	}

	public function onContentPrepare($context, &$article, &$params, $page = 0){
		if($context == 'com_content.article'){
			JPlugin::loadLanguage( 'plg_content_Jlvklike' );	


		global $jlvklikeinit;
		$apiId 				= $this->params->get('apiId');
		$width 				= $this->params->get('width');
		$autoAddOnlyFull 	= 1;//$this->params->get('autoAddOnlyFull') == 1 ? true : false;
		$position 			= $this->params->get('position');
		$max_words			= $this->params->get('desc_max_words');
		$type				= $this->params->get('type');
		$verb				= $this->params->get('verb');
		if (!isset($article->catid)) {
			$article->catid='';	
		}

		if (strpos($article->text,"{jlvklike_off}") !== false ) {
			$article->text = preg_replace("|{jlvklike.*?}|ixm","",$article->text);
			$article->text = preg_replace("|{jlvklike.*?}|ixm","",$article->text);
			$article->introtext = preg_replace("|{jlvklike.*?}|ixm","",$article->introtext);
		} else {
			$exceptcat = is_array($this->params->get('categories')) ? $this->params->get('categories') : array($this->params->get('categories'));

			if (!in_array($article->catid,$exceptcat)) {
				$view = JRequest::getCmd('view');

				if (($autoAddOnlyFull && $view == 'article') || !$autoAddOnlyFull) {
					//$mainframe = &JFactory::getApplication('site');
					$doc = JFactory::getDocument();
					if (!$jlvklikeinit) {
						$script = <<<HTML
							<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?1"></script>
							<script type="text/javascript">
							  VK.init({apiId: $apiId, onlyWidgets: true});
							</script>
HTML;
						$doc->addCustomTag($script);
						//$doc->addScriptDeclaration($script);
						$jlvklikeinit = true;
					}
					$pagehash = $article->id;
					$article->introtext = preg_replace("/\n\r|\r\n|\n|\r/", "", $article->introtext);
                    $article->introtext = preg_replace("/\t/", "", $article->introtext);
					$article->introtext = preg_replace("|<.*?>|ixm","",$article->introtext);
					$article->introtext = preg_replace("|{.*?}|ixm","",$article->introtext);
					$words = explode(" ",$article->introtext);
					$words_sliced = array_slice($words,0,intval($max_words));
					$article->introtext = implode(" ",$words_sliced)."...";
					$site = JURI::base();

					$link = $site.substr(JRoute::_(ContentHelperRoute::getArticleRoute($article->slug,$article->catid,$article->catslug)),1);

					$scriptPage = <<<HTML
						<br clear="all"><div id="jlvklike{$article->id}"></div>
						<script type="text/javascript">
						VK.Widgets.Like("jlvklike{$article->id}", {width: "$width", pageTitle: "{$article->title}", pageDescription: "{$article->introtext}", pageUrl: "{$link}", type: "{$type}", verb: "{$verb}"},$pagehash);
						</script>
HTML;
					if ($this->params->get('autoAdd') == 1) {
						switch($position){
							case "top":
								$article->text = $scriptPage.$article->text;
								break;
							case "bottom":
								$article->text .= $scriptPage;
								break;
						}

					} else {
						$article->text = str_replace("{jlvklike}",$scriptPage,$article->text);
					}


				}
			} else {
				$article->text = str_replace("{jlvklike}","",$article->text);
			}

		}


	}

}
}
